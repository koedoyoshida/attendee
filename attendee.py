#!/usr/bin/python
# -*- coding: utf-8 -*-

# od6 一般:connpass
# oda 一般:銀行振込
# od8 チュートリアル
# oco Party招待
# od0 スポンサー
# od2 スタッフ
# od3 チュートリアル:請求書
# od7 Patron
# ocy スピーカー確認
# ocq 業者

import time
import sys
import csv
import gdata.spreadsheet.service

email = 'koedoyoshida@gmail.com'

keynames = {
    u'category': 'category',
    u'参加枠名': 'type',
    u'ユーザー名': 'username',
    u'表示名': 'displayname',
    u'firstname': 'firstname',
    u'lastname': 'lastname',
    u'firstname名アルファベットで': 'firstname',
    u'lastname姓アルファベットで': 'lastname',
    u'Organization / 所属組織': 'organization',
    u'organization': 'organization',
    u'Email Address / メールアドレス': 'email',
    u'emailaddress': 'email',
    u'tシャツのサイズ': 'size',
    u't-shirtsizetシャツのサイズ': 'size',
    u't-shirt': 'size',
    u'lunchランチ': 'lunch',
    u'studentdiscountrate3000jpy学生料金3000円': 'student',
    u'参加ステータス': 'status',
    u'受付番号': 'number',
    u'fullname名札用': 'fullname',
    u'名前': 'displayname',
    u'時間': 'time',
    u'教室': 'room',
    u'人数': 'count',
}

OUTPUT = ('category', 'organization', 'fullname', 'lunch', 'username', 'firstname', 'lastname', 'email', 'size', 'student', 'number', 'speaker', 'time', 'room')

def speaker_check(attendee, speaker):
    # スピーカーだったら時間と場所を抜き出す
    room = None
    time = None
    try:
        fullname = attendee['fullname'].lower()

        for row in speaker:
            if row['fullname'].lower() == fullname:
                room = row['room']
                time = row['time']
                speaker.remove(row)
                break
    except:
        pass

    return room, time

def create_attendee_csv(attendee_list):
    writer = csv.writer(open('attendee-utf8.csv', 'wb'))
    writer.writerow(OUTPUT)
    for attendee in attendee_list:
        row = []
        for key in OUTPUT:
            value = attendee.get(key, '')
            if value == None:
                value = ''
            if key == 'size' and value.startswith('US'):
                us_, value = value.split('(')
                value = value.replace(')', '')
            row.append(value)
        writer.writerow(row)

def get_sheet_list(gd_client, spread_id):
    sheet_list = {}
    feed = gd_client.GetWorksheetsFeed(spread_id)
    for entry in feed.entry:
        sheet_id = entry.id.text.rsplit('/',1)[1]
        sheet_title = entry.content.text
        sheet_list[sheet_title] = sheet_id
    return sheet_list

def get_spread_id(gd_client, title):
    query = gdata.spreadsheet.service.DocumentQuery()
    query['title'] = title
    feed = gd_client.GetSpreadsheetsFeed(query=query)
    spread_id = feed.entry[0].id.text.rsplit('/',1)[1]
    return spread_id

def row_to_dict(row, category):
    # データを抜き出して辞書に入れる
    attender = {'category': category}
    for key in row.custom:
        keyname = keynames.get(key, None)
        if keyname != None:
            text = row.custom[key].text
            if isinstance(text, unicode):
                text = text.encode('utf-8')
                try:
                    text, dummy_ = text.split(' / ')
                except:
                    pass
            if keyname == 'fullname' and text != None:
                attender['fullname'] = text
                firstname, lastname = text.split(' ', 1)
                attender['firstname'] = firstname
                attender['lastname'] = lastname
            else:
                attender[keyname] = text
    # fullname を作る
    if not attender.has_key('fullname'):
        attender['fullname'] = "%s %s" % (attender['firstname'],
                                          attender['lastname'])
    return attender

def get_others(title, sheet_list, spread_id):
    sheet_id = sheet_list[title]
    rows = gd_client.GetListFeed(spread_id, sheet_id).entry
    others = []
    for row in rows:
        other = row_to_dict(row, 'Other')
        if other['count'] != None:
            count = int(other['count'])
            if count > 0:
                others.append(other)
                # 同伴者がいる場合は、その名札を作成
                for i in range(count - 1):
                    companion = {'category': other['category'],
                                 'organization': other['organization'],
                                 }
                    others.append(companion)
    return others

def get_attendee(title, sheet_list, spread_id, category='Attendee'):
    sheet_id = sheet_list[title]
    rows = gd_client.GetListFeed(spread_id, sheet_id).entry
    attendee = []
    for row in rows:
        attender = row_to_dict(row, category)
        if attender.get('status', '参加') == '参加':
            if attender.get('firstname', None) != None:
                attendee.append(attender)
    return attendee

if __name__ == '__main__':
    password = sys.argv[1]
    gd_client = gdata.spreadsheet.service.SpreadsheetsService(email, password)
    gd_client.ProgrammaticLogin()

    title = 'PyCon JP 2014 参加者一覧'
    spread_id = get_spread_id(gd_client, title)
    sheet_list = get_sheet_list(gd_client, spread_id)

    sheet_info = {
        'connpass': ('connpass', None),
        'bank': ('別途払込', None),
#        'tutorial': ('チュートリアル', 'Tutorial'),
#        'tutorial-bank': ('チュートリアル(別途払込)', 'Tutorial'),
#        'sponsor': ('スポンサー', 'Sponsor'),
#        'staff': ('スタッフ', 'STAFF'),
#        'speaker': ('Speaker', 'Speaker'),
#        'invite': ('招待', None),
        }
    participant = {}

    # シートからデータを読み込む
    for key, value in sheet_info.iteritems():
        title, category = value
        print key 
        if key == 'other':
            participant[key] = get_others(title, sheet_list, spread_id)
        else:
            if category == None:
                participant[key] = get_attendee(title, sheet_list, spread_id)
            else:
                participant[key] = get_attendee(title, sheet_list, spread_id,
                                                category=category)
    # 人数確認
    #for key, value in participant.iteritems():
    #    print key, len(value)

    # 名札用全参加者リスト
    attendee_list = []
#    for key in ('connpass', 'bank', 'tutorial', 'tutorial-bank', 'sponsor',
#                'patron', 'invite', 'staff', 'other'):
#        attendee_list.extend(participant[key])
    for key in ('connpass', 'bank'):
        attendee_list.extend(participant[key])

#    speaker = participant['speaker']
    #print 'attendee', len(attendee_list)
    #print 'party', len(party)
    #print 'speaker', len(speaker)
#    for attendee in attendee_list:
#         Speaker チェック
#        room, time = speaker_check(attendee, speaker)
#        if room:
#            attendee['speaker'] = 'Speaker'
#            attendee['room'] = room
#            attendee['time'] = time

    create_attendee_csv(attendee_list)
#    print "speaker"
#    for row in speaker:
#        print ','.join([row['fullname'], row['room'], row['time']])
